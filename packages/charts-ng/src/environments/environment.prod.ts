export const environment = {
  production: true,
  host: location.hostname === 'localhost' ? 'http://localhost:8000' : 'http://app.wbstat.xyz',
};
