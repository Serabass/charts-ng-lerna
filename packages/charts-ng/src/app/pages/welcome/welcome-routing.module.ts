import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WelcomeComponent} from './welcome.component';
import {CommonModule} from '@angular/common';
import {GoodPageComponent} from './good-page/good-page.component';

const routes: Routes = [
  {path: '', component: WelcomeComponent},
  {path: 'product/:id', component: GoodPageComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class WelcomeRoutingModule {
}
