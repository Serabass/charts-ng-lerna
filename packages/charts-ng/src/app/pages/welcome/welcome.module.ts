import {NgModule} from '@angular/core';
import {WelcomeRoutingModule} from './welcome-routing.module';
import {WelcomeComponent} from './welcome.component';
import {NzTableModule} from 'ng-zorro-antd/table';
import {CommonModule} from '@angular/common';
import {NzDividerModule} from 'ng-zorro-antd/divider';
import {GoodPageComponent} from './good-page/good-page.component';
import {ChartComponent} from './chart/chart.component';
import {NzLayoutModule} from 'ng-zorro-antd/layout';
import {NzGridModule} from 'ng-zorro-antd/grid';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzPaginationModule} from 'ng-zorro-antd/pagination';

@NgModule({
  imports: [
    WelcomeRoutingModule,
    NzLayoutModule,
    NzGridModule,
    NzTableModule,
    NzDividerModule,
    CommonModule,
    NgxChartsModule,
    NzPaginationModule,
    NzButtonModule,
    NzIconModule
  ],
  declarations: [WelcomeComponent, GoodPageComponent, ChartComponent],
  exports: [WelcomeComponent]
})
export class WelcomeModule {
}
