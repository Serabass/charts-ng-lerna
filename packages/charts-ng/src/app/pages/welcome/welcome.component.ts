import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  public loading = false;
  public page = 1;
  public pageSize = 15;
  public total = 0;

  public goods: any = [];

  public res: any;

  constructor(public http: HttpClient, private route: ActivatedRoute) {
  }

  public load(
    pageIndex: number): void {
    const params: any = {
      page: pageIndex,
    };

    this.route.queryParams.subscribe(({shop_id}) => {
      if (!!shop_id) {
        params.shop_id = shop_id;
      }

      this.loading = true;
      this.http.get(`${environment.host}/api/good/all`, {
        params
      }).subscribe((res: any) => {
        this.res = res;
        this.goods = res.data;
        this.total = res.total;
        this.pageSize = res.per_page;
        this.loading = false;
      });
    });
  }

  ngOnInit(): void {
    this.load(this.page);
  }
}
