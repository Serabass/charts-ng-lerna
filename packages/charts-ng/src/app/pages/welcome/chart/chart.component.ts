import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import { NzButtonType } from 'ng-zorro-antd/button';
import { environment } from 'src/environments/environment';

enum ChartPeriod {
  HOUR = 'hour',
  DAY = 'day',
  ONE_MIN = '1min',
  TEN_MIN = '10min',
}

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  public ChartPeriod = ChartPeriod;

  public loading = false;

  @Input()
  public good: any;
  public period: ChartPeriod = ChartPeriod.HOUR;

  data: any[] = [];
  view: [number, number] = [1000, 500];

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Дата';
  yAxisLabel: string = 'Количество';
  timeline: boolean = true;

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  constructor(private http: HttpClient) {
  }

  onSelect(data: any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data: any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

  public load(period = this.period): void {
    this.loading = true;
    this.period = period;
    this.http.get(`${environment.host}/api/product/${this.good.id}/chart/${this.period}`).subscribe((res: any) => {
      this.data = res;
      this.loading = false;
    });
  }

  public ngOnInit(): void {
    this.load();
  }

  public getButtonType(name: string): NzButtonType {
    if (this.period === name) {
      return 'primary';
    }

    return 'default';
  }
}
