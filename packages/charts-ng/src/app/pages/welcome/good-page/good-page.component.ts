import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router, RouterState} from '@angular/router';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-good-page',
  templateUrl: './good-page.component.html',
  styleUrls: ['./good-page.component.scss']
})
export class GoodPageComponent implements OnInit {

  public good: any;

  constructor(public http: HttpClient, public route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.http.get(`${environment.host}/api/product/${params.id}`).subscribe((res: any) => {
        this.good = res;
      });
    });
  }
}
