import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-welcome',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.scss']
})
export class ShopsComponent implements OnInit {

  public loading = false;


  public goods: any[] = [];

  constructor(public http: HttpClient) { }

  ngOnInit(): void {
    this.loading = true;
    this.http.get(`${environment.host}/api/shop/all`).toPromise<any>().then((res: any[]) => {
      this.goods = res;
      this.loading = false;
    });
  }
}
