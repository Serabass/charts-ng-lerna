import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ShopsComponent} from './shops.component';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {path: '', component: ShopsComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class ShopsRoutingModule {
}
