import {NgModule} from '@angular/core';
import {ShopsRoutingModule} from './shops-routing.module';
import {ShopsComponent} from './shops.component';
import {NzTableModule} from 'ng-zorro-antd/table';
import {CommonModule} from '@angular/common';
import {NzDividerModule} from 'ng-zorro-antd/divider';

@NgModule({
  imports: [
    ShopsRoutingModule,
    NzTableModule,
    NzDividerModule,
    CommonModule,
  ],
  declarations: [ShopsComponent],
  exports: [ShopsComponent]
})
export class ShopsModule {
}
