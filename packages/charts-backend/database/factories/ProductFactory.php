<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->company,
            'photo_url' => $this->faker->imageUrl(),
            'vendor_code' => $this->faker->numberBetween(1, 5),
            'shop_id' => Shop::inRandomOrder()->first()->id,
        ];
    }
}
