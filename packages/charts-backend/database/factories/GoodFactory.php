<?php

namespace Database\Factories;

use App\Models\Good;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Factories\Factory;

class GoodFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Good::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => Product::inRandomOrder()->first()->id,
            'shop_id' => Shop::inRandomOrder()->first()->id,
            'count' => $this->faker->numberBetween(0, 100),
            'created_at' => $this->faker->dateTimeInInterval('-5 hours', '+5 hours'),
            'updated_at' => $this->faker->dateTimeInInterval('-5 hours', '+5 hours')
        ];
    }
}
