<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Shop::factory(500)->create();
        \App\Models\Product::factory(500)->create();
        \App\Models\Good::factory(5000)->create();
    }
}
