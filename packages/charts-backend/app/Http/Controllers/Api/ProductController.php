<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Good;
use App\Models\Product;

class ProductController extends Controller
{
    const PERIOD_HOUR = 'hour';
    const PERIOD_DAY = 'day';
    const PERIOD_10MIN = '10min';
    const PERIOD_1MIN = '1min';

    public function item(int $id)
    {
        return Product::whereId($id)->first();
    }

    public function chart(int $id, string $period = 'hour')
    {
        $product = Product::find($id);

        switch ($period) {
            case self::PERIOD_10MIN:
                $groupBy = 'CONCAT(LEFT(DATE_ADD(created_at, interval 3 hour), 15), "0:00")';
                break;
            case self::PERIOD_HOUR:
                $groupBy = 'CONCAT(LEFT(DATE_ADD(created_at, interval 3 hour), 13), ":00:00")';
                break;
            case self::PERIOD_DAY:
                $groupBy = 'CONCAT(LEFT(DATE_ADD(created_at, interval 3 hour), 10), " 00:00:00")';
                break;
            default:
                $groupBy = 'CONCAT(LEFT(DATE_ADD(created_at, interval 3 hour), 16), ":00")';
                break;
        }

        $list = Good::whereProductId($product->id)
            ->select(\DB::raw('MAX(count) as value'), \DB::raw($groupBy . ' as name'))
            ->groupBy(\DB::raw($groupBy))
            ->get();

        $labels = [];

        foreach ($list as $row) {
            if (!in_array($row->hour, $labels)) {
                $labels[] = $row->hour;
            }
        }

        return [
            [
                'name' => $product->title,
                'series' => $list
            ]
        ];
    }
}
