<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function all()
    {
        return Shop::all();
    }

    public function list() {
        $shops = Shop::all();

        $result = [];

        foreach ($shops as $shop) {
            $result[] = join('|', [$shop->id, $shop->title, $shop->url]);
        }

        return join("\r\n", $result);
    }
}
