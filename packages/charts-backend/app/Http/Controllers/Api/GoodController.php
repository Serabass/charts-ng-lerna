<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Good;
use App\Models\Product;
use Illuminate\Http\Request;

class GoodController extends Controller
{
    public function all(Request $request)
    {
        $shop_id = $request->input('shop_id');

        $q = "SELECT t1.* FROM goods t1
  JOIN (SELECT product_id, MAX(id) id FROM goods GROUP BY product_id) t2
    ON t1.id = t2.id AND t1.product_id = t2.product_id";

        $query = Good::query()
            ->with('shop')
            ->with('product')
            ->join(\DB::raw('(SELECT product_id, MAX(id) id FROM goods GROUP BY product_id) t2'), function ($join) {
                $join->on('goods.id', '=', 't2.id')
                    ->on('goods.product_id', '=', 't2.product_id');
            })
            ->orderBy('goods.product_id');

        // $query = Good::query()
        //     ->addSelect(\DB::raw('MAX(count) as count'))
        //     ->addSelect('product_id')
        //     ->addSelect('shop_id')
        //     ->with('shop')
        //     ->with('product')
        //     ->groupBy('product_id')
        // ;

        if (!empty($shop_id)) {
            $products = Product::whereShopId($shop_id)->pluck('id');
            $query = $query->whereIn('t2.product_id', $products);
        }

        return $query->paginate();
    }

    public function bulkInsert(Request $request)
    {
        $body = $request->getContent();
        $shop_id = $request->get('shop');
        $body = trim($body, '|');
        $result = [];

        $lines = explode('|', $body);

        foreach ($lines as $line) {
            $explode = explode(';', $line);

            list($title, $imageUrl, $vendor_code, $count) = $explode;

            if (Product::whereVendorCode($vendor_code)->whereShopId($shop_id)->exists()) {
                $product = Product::whereVendorCode($vendor_code)
                    ->whereShopId($shop_id)
                    ->first();
            } else {
                $product = new Product();
            }

            $product->title = urldecode(trim($title));
            $product->photo_url = trim($imageUrl);
            $product->vendor_code = trim($vendor_code);
            $product->shop_id = $shop_id;
            $product->save();

            // if (Good::whereProductId($product->id)->whereShopId($shop_id)->exists()) {
            //     $good = Good::whereProductId($product->id)
            //         ->whereShopId($shop_id)
            //         ->first();
            // } else {
            $good = new Good();
            // }

            $good->product_id = $product->id;
            $good->shop_id = $shop_id;
            $good->count = trim($count);
            $good->save();

            $result[] = $product->id;
        }

        return join("\r\n", $result);
    }
}
