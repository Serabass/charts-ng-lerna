<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Good
 *
 * @property int $id
 * @property string $title
 * @property string $photo_url
 * @property string $vendor_code
 * @property int $count
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Good newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Good newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Good query()
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Good wherePhotoUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereVendorCode($value)
 * @mixin \Eloquent
 * @property string $shop_name
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereShopName($value)
 * @property string $shop_id
 * @property-read \App\Models\Shop $shop
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereShopId($value)
 * @property string $product_id
 * @method static \Illuminate\Database\Eloquent\Builder|Good whereProductId($value)
 */
class Good extends Model
{
    use HasFactory;

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    // public function save(array $options = [])
    // {
    //     Good::where('vendor_code', '=', $this->vendor_code)
    //         ->where('id', '!=', $this->id)
    //         ->update([
    //             'title' => $this->title,
    //             'photo_url' => $this->photo_url,
    //         ]);
//
    //     return parent::save();
    // }
}
