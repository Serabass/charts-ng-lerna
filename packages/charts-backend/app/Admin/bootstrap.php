<?php

use SleepingOwl\Admin\Model\ModelConfiguration;

// PackageManager::load('admin-default')
//    ->css('extend', public_path('packages/sleepingowl/default/css/extend.css'));

AdminSection::registerModel(\App\Models\Shop::class, function (ModelConfiguration $model) {
    $model->setTitle('Магазины');
    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::link('title')->setLabel('Title')->setWidth('400px'),
            AdminColumn::text('url')->setLabel('URL')->setWidth('400px'),
        ]);
        $display->paginate(15);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function() {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('title', 'Title')->required(),
            AdminFormElement::text('url', 'URL')->required()->unique(),
        );
        return $form;
    });
})
    ->addMenuPage(\App\Models\Shop::class, 0)
    ->setIcon('fa fa-bank');

AdminSection::registerModel(\App\Models\Product::class, function (ModelConfiguration $model) {
    $model->setTitle('Товары');
    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::image('photo_url')->setLabel('Изображение')->setWidth('400px'),
            AdminColumn::link('title')->setLabel('Наименование')->setWidth('400px'),
            AdminColumn::text('vendor_code')->setLabel('Артикул')->setWidth('400px'),
            AdminColumn::relatedLink('shop.title')->setLabel('Магазин')->setWidth('400px'),
            AdminColumn::datetime('created_at')->setLabel('Создан')->setWidth('400px'),
        ]);
        $display->paginate(15);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function() {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('title', 'Title')->required(),
            AdminFormElement::image('photo_url', 'Изображение'),
            AdminFormElement::text('vendor_code', 'Артикул')
        );
        return $form;
    });
})
    ->addMenuPage(\App\Models\Product::class, 0)
    ->setIcon('fa fa-bank');

AdminSection::registerModel(\App\Models\Good::class, function (ModelConfiguration $model) {
    $model->setTitle('История');
    // Display
    $model->onDisplay(function () {
        $display = AdminDisplay::table()->setColumns([
            AdminColumn::relatedLink('product.title')->setLabel('Товар')->setWidth('400px'),
            AdminColumn::relatedLink('shop.title')->setLabel('Магазин')->setWidth('400px'),
            AdminColumn::text('count')->setLabel('Кол-во')->setWidth('400px'),
            AdminColumn::datetime('created_at')->setLabel('Создан')->setWidth('400px'),
        ]);
        $display->paginate(15);
        return $display;
    });
    // Create And Edit
    $model->onCreateAndEdit(function() {
        $form = AdminForm::panel()->addBody(
            AdminFormElement::text('title', 'Title')->required(),
            AdminFormElement::image('photo_url', 'Изображение'),
            AdminFormElement::text('vendor_code', 'Артикул')
        );
        return $form;
    });
})
    ->addMenuPage(\App\Models\Good::class, 0)
    ->setIcon('fa fa-bank');
