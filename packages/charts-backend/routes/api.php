<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')
    ->prefix('/shop')
    ->group(function () {
        Route::get('/all', 'App\Http\Controllers\Api\ShopController@all');
        Route::get('/list', 'App\Http\Controllers\Api\ShopController@list');
    });

Route::middleware('api')
    ->prefix('/good')
    ->group(function () {
        Route::get('/all', 'App\Http\Controllers\Api\GoodController@all');
        Route::post('/bulk-insert', 'App\Http\Controllers\Api\GoodController@bulkInsert');
        Route::get('/{id}/chart', 'App\Http\Controllers\Api\GoodController@chart');
    });

Route::middleware('api')
    ->prefix('/product')
    ->group(function () {
        Route::get('/{id}', 'App\Http\Controllers\Api\ProductController@item');
        Route::get('/{id}/chart/{period?}', 'App\Http\Controllers\Api\ProductController@chart');
    });
